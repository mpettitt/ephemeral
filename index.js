var app = require('http').createServer(handler)
var io = require('socket.io')(app);
var fs = require('fs');
var sanitize = require('sanitize-caja');

var users = new Array();

app.listen(8080);

/**
 * Hash function from http://stackoverflow.com/a/7616484
 * Used to generate a distinct integer representation of the socket id
 *
 * @returns {Number} Hash of input
 */
String.prototype.hashCode = function() {
  var hash = 0, i, chr, len;
  if (this.length == 0) return hash;
  for (i = 0, len = this.length; i < len; i++) {
    chr   = this.charCodeAt(i);
    hash  = ((hash << 5) - hash) + chr;
    hash |= 0; // Convert to 32bit integer
  }
  return hash;
};

/**
 * Generates a distinct colour for each user, based on socket id
 * This means that even if a user changes their name, previous comments
 * are still attached to them. Also, it makes following conversations
 * easier, since you can see different speakers at a glance
 *
 * @param {string} socketid The server generated socketid
 * @returns {string} An RGB color value, in CSS format
 */
function generateUserColor(socketid){
  var socketid = (typeof socketid !== 'undefined' && socketid !== null) ? socketid : '';
  var userhash = socketid.toString().hashCode();
  var usercol = '#'+Math.abs(userhash % 16777215).toString(16);
  if (usercol.length !=7){
    var padding = '000000';
    var padlength = 7 - usercol.length;
    usercol += padding.slice(0,padlength);
  }
  return usercol;
}

/**
 * Clears out the users list by checking through active connections
 * and removing any users which don't correspond to active connections
 *
 * @param {Array} activeConns List of active connections
 */
function pruneUsers(activeConns){
  for (i=0; i<users.length; i++){
    for (j=0; j<activeConns.length; j++){
      if (users[i].socketid == activeConns[j].id){
        users[i] = null;
      }
    }
  }
  users.sort();
}

/**
 * Really minimal http routing function. Returns the index.html file
 * for all requests, except for when the script.js file is specifically
 * requested
 *
 * @param {Object} req HTTP Request object
 * @param {Object} res HTTP Response object
 */
function handler (req, res) {
  if (req.url == '/script.js'){
    fs.readFile(__dirname + '/script.js',
    function (err, data) {
      if (err) {
        res.writeHead(500);
        return res.end('Error loading script.js');
      }

      res.writeHead(200);
      res.end(data);
    })
  } else {
    fs.readFile(__dirname + '/index.html',
    function (err, data) {
      if (err) {
        res.writeHead(500);
        return res.end('Error loading index.html');
      }

      res.writeHead(200);
      res.end(data);
    });
  }
}

io.on('connection', function (socket) {
  /*
   * Handles initial connection from client, adding them to the user
   * list, giving them an initial userid, and the list of existing users
   * and updating all other users that a new user has joined
   */
  var userObj = {socketid: socket.id, userid: 'User'+users.length, usercol: generateUserColor(socket.id)};
  users.push(userObj);
  socket.emit('userid', userObj);
  socket.emit('userlist', users);
  socket.broadcast.emit('userlist', users);
  socket
    .on('message', function(data){
      /*
       * Whenever a message is sent from a client, attaches the server
       * details of the user, adds a timestamp, then sends it out
       * first to the originating user, then to all other users
       */
      data.user = userObj;
      data.timestamp = Date.now();
      data.message = sanitize(data.message);
      socket.emit('message', data);
      socket.broadcast.volatile.emit('message', data);
    })
    .on('username', function(data){
      /*
       * Whenever a username update is received from a client, updates
       * the server details of the user, responds to the originator with
       * updated details, then tells all other users
       */
      for (i=0; i<users.length; i++){
        if (users[i].socketid == socket.id){
          users[i].userid = sanitize(data.username);
          socket.emit('userid', users[i]);
          socket.emit('updateuserid', users[i]);
          socket.broadcast.emit('updateuserid', users[i]);
        }
      }
    })
    .on('disconnect', function(data){
      /*
       * Whenever a client disconnects, remove them from the server
       * list then let the other users know
       */
      for (i=0; i<users.length;i++){
        if (users[i].socketid == socket.id){
          users = users.slice(0,i).concat(users.slice(i+1));
          break;
        }
      }
      pruneUsers(io.sockets.connected);
      socket.broadcast.emit('userlist', users);

    });
});

if (typeof exports !== 'undefined'){
  exports.generateUserColor = generateUserColor;
}
