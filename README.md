# README - Ephemeral, a lightweight chat system #

Ephemeral is a very lightweight in-browser chat system, using websockets (through sockets.io). It doesn't use a database, doesn't log chats in any way, and scales

## Features

* Easy setup
* Usernames which can be updated on the fly, and which are remembered between sessions
* Persistent user colouring, so even if you change your name, you're still linked to previous statements
* Automatic removal of old messages after 60 seconds
* Detects server being unreachable and attempts to resolve (presenting "Server gone away message" on chat - this will not be removed until the server is back)
* Attempts to prevent XSS attacks by filtering all user input

## Known issues

* Fast refreshing of browser will cause the user list to keep growing. This will sort itself out after a while when the server realises that the clients are gone (the missing users will be removed after about a minute)
* Doesn't handle port 8080 being in use well

## Requirements

* Nodejs
* Socket.io (`npm install socket.io` - this doesn't need to be globally installed)
* Sanitize-caja (`npm install sanitize-caja`)

## Running Ephemeral

Ensure you have a firewall rule allowing connections to port 8080 then run

    nodejs index.js

If you want to run it forever, use the Node package forever (you may need to be root to install packages globally):

    npm install forever -g
    forever start index.js

## Testing

    mocha
