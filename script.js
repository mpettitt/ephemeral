var curLoc = window.location;
console.log(curLoc.origin);
var socket = io(curLoc.origin);
socket
  .on('connect', function(data){
    /*
     * When this client connects to the server, remove any system alerts,
     * check for a username in localStorage, and if there is one, trigger
     * changing to it
     */
    $('[data-timestamp=system]').remove();
    var username = localStorage.getItem('username');
    if (username != null){
      changeUsername(username);
    }
  })
  .on('userid', function(data){
    /*
     * When the server confirms this client's username has changed,
     * update the username in the head of the page
     */
    console.group('userid event');
    console.log(JSON.stringify(data));
    console.groupEnd();
    $('#userid').html(data.userid);
  })
  .on('userlist', function(data){
    /*
     * When the server sends an updated list of users, clear the existing
     * list of users, and insert the updated list
     */
    console.group('userlist event');
    console.log(JSON.stringify(data));
    console.groupEnd();
    $('#userlist').empty();
    data.forEach(function(user){
      var userLab = $('<div class="label" data-socketid="'+user.socketid+'">'+user.userid+'</div>');
      userLab.css("backgroundColor", user.usercol);
      $('#userlist').append(userLab);
    })
  })
  .on('updateuserid', function(data){
    /*
     * When the server tells this client of another user changing username,
     * find all the places where their username is, and change it to the
     * updated name
     */
    console.group('updateuserid event');
    console.log(data);
    console.groupEnd();
    $('.container').find('[data-socketid='+data.socketid+']').html(data.userid);
  })
  .on('message', function(data){
    /*
     * When a new message is received from the server, call the newMessage
     * function. There is no distinction between messages from this client
     * and from other clients
     */
    console.log(data);
    newMessage(data);
  })
  .on('disconnect', function(data){
    /*
     * When a disconnect signal is received from the websocket library,
     * put a system message on the chat pane. The websocket library will
     * keep trying to reconnect until the server comes back
     */
    console.log('Server gone away');
    newMessage({ message: "Server gone away - attempting to reconnect", user: {userid: 'Ephemeral chat', usercol: '#ff0000'}, timestamp: 'system'});
  });

/**
 * Handle adding messages to the chat pane.
 * Message object is a JSON object comprising of:
 *  - message (whatever the sending user typed)
 *  - timestamp (added by the server)
 *  - user object (username, socketid, user color)
 *
 * @param {Object} data Message object from the server
 */
function newMessage(data){
  var structure = $('<div class="list-group-item"><h4 class="list-group-item-heading">List group item heading</h4><p class="list-group-item-text">...</p></div>');
  structure
    .attr('data-timestamp', data.timestamp)
    .find('h4')
      .html(data.user.userid)
      .attr('data-socketid', data.user.socketid)
    .end()
    .find('p')
      .html(data.message)
    .end()
    .css("border-left", "10px solid "+data.user.usercol);
  $('#chat').append(structure);
}

/**
 * Remove old messages from the chat pane. As messages are intended
 * to be ephemeral, there is no history of messages
 *
 */
function clearOld(){
  var now = Date.now();
  $('[data-timestamp]').each(function(curMessage){
    if ($(this).attr('data-timestamp') < now - 60000){
      $(this).hide('slow', function(){ $(this).remove();});
    }
  })
}

/**
 * Handle sending messages
 * Checks whether the message entry box is empty, and if not,
 * sends the message to the server. It then sets the placeholder
 * for the message entry box to the sent message, and the value
 * of it to an empty string, so the user can see their previous
 * message, but still type a new one without needing to delete
 * anything
 *
 */
function sendMessage(){
  if ($('#entry').val().length>0){
    socket.emit('message', { message: $('#entry').val()});
  }
  $('#entry').attr('placeholder', $('#entry').val()).val('');
}

/**
 * Handle changing the username. Sets the name in the header,
 * tells the server about the new username, and stores it in
 * localStorage
 *
 * @param {username} data Message object from the server
 */
function changeUsername(username){
  $('#userid').html(username);
  socket.emit('username', { username: username});
  localStorage.setItem("username", username);
}

$(document).ready(function(){
  /*
   * Perform garbage collection of old messages every second
   */
  var gc = setInterval(clearOld, 1000);
  $('body')
    // On clicking the Send button
    .on('click', '#send', sendMessage)
    // Or pressing enter in the message entry box
    .on('keyup', '#entry', function(event){
      if (event.keyCode == 13){
        // Send the message
        sendMessage();
      }
    })
    // On blurring the username box
    .on('change', '#username', function(){
      // Change the username
      changeUsername($('#username').val());

    })
    .on('beforeunload', function(){
      // When leaving the page, send a disconnect message
      socket.emit('disconnect', {message: 'Bye'});
    })
})
