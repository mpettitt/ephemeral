var assert = require("assert");
var index = require("../index.js");

describe('index', function(){
  describe('generateUserColor', function(){
    it('should return the same output for any given input', function(){
      assert.equal(index.generateUserColor('abc123'), "#e72dc4");
      assert.equal(index.generateUserColor(1), "#310000");
    });
    it('should handle null socketids', function(){
      assert.equal(index.generateUserColor(null), "#000000");
    })
    it('should handle empty socketids', function(){
      assert.equal(index.generateUserColor(), "#000000");
    })
  });
})
